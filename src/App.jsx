import { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CourseForm from './components/CourseForm/CourseForm';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { BrowserRouter } from 'react-router-dom/cjs/react-router-dom.min';
import Test from './Test';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';

function App() {
	const [show, setShow] = useState(false);
	return (
		<BrowserRouter>
			<Router>
				<Switch>
					<Route exact path='/'>
						<div>
							<Header />
							<Login />
						</div>
					</Route>
					<Route path='/registration'>
						<Header />
						<Registration />
					</Route>
					<Route path='/login'>
						<Header />
						<Login />
					</Route>
					<Route exact path='/courses'>
						<Header />
						<Courses />
					</Route>
					<PrivateRoute exact path='/courses/add' component={CourseForm} />
					<Route
						exact
						path='/courses/:courseId'
						children={
							<div>
								<Header />
								<CourseInfo />
							</div>
						}
					></Route>
					<PrivateRoute
						exact
						path='/courses/update/:courseId'
						component={CourseForm}
					/>
					<Route exact path='/test'>
						<Test />
					</Route>
				</Switch>
			</Router>
		</BrowserRouter>
	);
}
export default App;
