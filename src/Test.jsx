import store from './store';
import { useState, useEffect } from 'react';
import { addCoursesAction } from './store/courses/reducer';
import { v4 as uuidv4 } from 'uuid';
import { addUserAction, removeUserAction } from './store/user/reducer';
import Button from './common/Button/Button';
import { getCoursesAction } from './store/courses/reducer';
import { mockedAuthorsList } from './constants';
import Input from './common/Input/Input';
import './test.css';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

function Test() {
	const getCourses = async () => {
		const courses = fetch('http://localhost:4000/courses/all')
			.then((response) => response.json())
			.then((courses) => {
				return courses;
			});
		const a = await courses;
		console.log(a);
	};

	let authorList;
	const getAuthors = () => {
		const authors = fetch('http://localhost:4000/authors/all')
			.then((response) => response.json())
			.then((authors) => {
				return authors;
			});
		authors.then((a) => {
			authorList = a.result;
			console.log(a);
		});
	};

	const userLogin = {
		name: '',
		email: 'admin@email.com',
		password: 'admin123',
	};

	let token;
	const postLogin = async () => {
		const login = fetch('http://localhost:4000/login', {
			method: 'POST',
			body: JSON.stringify(userLogin),
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then((response) => response.json())
			.then((result) => result);
		const a = await login;
		console.log(a);
		token = a.result;
		console.log(token);
	};

	const course1 = {
		title: 'Course1',
		description: 'blabl bal balbalbmllbalbab lalla ',
		duration: 430,
		authors: [authorList[2].id, authorList[1].id],
	};

	const postCourse = async () => {
		const addCourse = fetch('http://localhost:4000/courses/add', {
			method: 'POST',
			body: JSON.stringify(course1),
			headers: {
				'Content-Type': 'application/json',
				Authorization: token,
			},
		})
			.then((response) => response.json())
			.then((result) => result);

		const a = await addCourse;
		console.log(a);
	};

	return (
		<div>
			<h1>Test</h1>
			<Button
				type='button'
				buttonText='Get courses'
				trigger={() => getCourses()}
			/>
			<Button
				type='button'
				buttonText='Get authors'
				trigger={() => getAuthors()}
			/>
			<div>
				<Button type='button' buttonText='Login' trigger={() => postLogin()} />
			</div>
			<div>
				<Button
					type='button'
					buttonText='Add course'
					trigger={() => postCourse()}
				/>
			</div>
		</div>
	);
}
export default Test;
