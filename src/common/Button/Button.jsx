import { clickHandler } from '../../components/Courses/Courses';

function Button(props) {
	return (
		<button
			className={props.className}
			type={props.type}
			onClick={props.trigger}
		>
			{props.buttonText}
		</button>
	);
}
export default Button;
