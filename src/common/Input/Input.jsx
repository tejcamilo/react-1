function Input(props) {
	return (
		<div>
			<label htmlFor={props.htmlFor}>{props.labelText}</label>
			<input
				type={props.type}
				id={props.id}
				name={props.name}
				placeholder={props.placeholderText}
				defaultValue={props.defaultValue}
				value={props.value}
				onChange={props.onChange}
				size={props.size}
			/>
		</div>
	);
}

export default Input;
