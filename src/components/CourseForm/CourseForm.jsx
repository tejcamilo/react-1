import { useState } from 'react';
import Button from '../../common/Button/Button';
import { mockedAuthorsList, mockedCoursesList } from '../../constants';
import { v4 as uuidv4 } from 'uuid';
import { useHistory } from 'react-router-dom';
import Input from '../../common/Input/Input';
import './courseform.css';
import store from '../../store';
import { addAuthorAction } from '../../store/author/reducer';
import {
	addCoursesAction,
	updateCoursesAction,
} from '../../store/courses/reducer';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { postCourse, putCourse } from '../../store/courses/thunk';
import { postAuthor } from '../../store/author/thunk';

// function CreateCourse(props) {
// const [course, setCourse] = useState('');
// const { courseId } = useParams();

// useEffect(() => {
// 	const newCourse = mockedCoursesList.find(
// 		(course) => course.id === courseId
// 	);

// 	setCourse(newCourse);
// }, []);

// let mockedCoursesList = store.getState().courses;
// let mockedAuthorsList = store.getState().author;

// const twoDigits = (num) => {
// 	return num.toString().padStart(2, '0');
// };
// console.log('f', course);

// const [title, setTitle] = useState('');
// const [description, setDescription] = useState('');
// const [duration, setDuration] = useState('');

// const date = new Date();
// let obj = {
// 	id: uuidv4(),
// 	title: '',
// 	description: '',
// 	creationDate: `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`,
// 	duration: '',
// 	authors: [],
// };

// let tempCourseAuthor = [];

// const [authorList, setAuthorList] = useState(mockedAuthorsList);

// const [courseAuthor, setCourseAuthor] = useState([]);

// let addCourseAuthor = (author) => {
// 	setCourseAuthor([...courseAuthor, author]);
// };

// const removeCourseAuthor = (author) => {
// 	console.log(author);
// 	console.log(tempCourseAuthor);
// 	const filter = courseAuthor.filter((item) => item.name !== author.name);
// 	setCourseAuthor(filter);
// };

// const createAuthor = (author) => {
// 	const newObj = { id: uuidv4(), name: author };
// 	addAuthorAction.payload = newObj;
// 	store.dispatch(addAuthorAction);
// 	console.log(store.getState());
// 	setAuthorList([...mockedAuthorsList, newObj]);
// 	mockedAuthorsList.push(newObj);
// };

// let history = useHistory();

// const testing = () => {
// 	obj.title = document.getElementById('title').value;
// 	obj.description = document.getElementById('description').value;
// 	obj.duration = Number(document.getElementById('duration').value);
// 	if (obj.title === '' || obj.description === '' || duration === '') {
// 		alert('Please fill in all fields');
// 	} else {
// 		addCoursesAction.payload = obj;
// 		store.dispatch(addCoursesAction);
// 		console.log(store.getState());
// 		tempCourseAuthor = courseAuthor;
// 		tempCourseAuthor.map((author) => obj.authors.push(author.id));
// 		mockedCoursesList.push(obj);
// 		console.log(obj);
// 		console.log(mockedAuthorsList);
// 		console.log(mockedCoursesList);
// 		history.push(`/courses`);
// 	}
// };

// const updateCourse = () => {
// 	obj.id = course.id;
// 	obj.title = document.getElementById('title').value;
// 	obj.description = document.getElementById('description').value;
// 	obj.duration = Number(document.getElementById('duration').value);
// 	obj.creationDate = course.creationDate;

// 	if (obj.title === '' || obj.description === '' || duration === '') {
// 		alert('Please fill in all fields');
// 	} else {
// 		addCoursesAction.payload = obj;
// 		store.dispatch(addCoursesAction);
// 		console.log(store.getState());
// 		tempCourseAuthor = courseAuthor;
// 		tempCourseAuthor.map((author) => obj.authors.push(author.id));
// 		mockedCoursesList.push(obj);
// 		console.log(obj);
// 		console.log(mockedAuthorsList);
// 		console.log(mockedCoursesList);
// 		//history.push(`/courses`);
// 	}
// };

// let renderAuthorList = authorList.map((author) => {
// 	return (
// 		<div key={author.id}>
// 			<p>{author.name}</p>
// 			<Button
// 				type='button'
// 				buttonText='Add author'
// 				trigger={() => {
// 					addCourseAuthor(author);
// 				}}
// 			/>
// 		</div>
// 	);
// });

// let renderCourseAuthors = courseAuthor.map((author) => {
// 	return (
// 		<div key={author.id}>
// 			<p>{author.name}</p>
// 			<Button
// 				type='button'
// 				buttonText='Remove author'
// 				trigger={() => {
// 					removeCourseAuthor(author);
// 				}}
// 			/>
// 		</div>
// 	);
// });
// let arr = [];

// if (courseId === undefined) {
// 	return (
// 		<div className='createcourse'>
// 			<form>
// 				<div className='title'>
// 					<div className='input'>
// 						<label htmlFor='title'>Title</label>
// 						<input
// 							type='text'
// 							id='title'
// 							placeholder='Enter title...'
// 							size='50'
// 						/>
// 					</div>
// 					<div className='btn'>
// 						<Button
// 							buttonText='Create course'
// 							trigger={() => {
// 								testing();
// 								//props.trigger();
// 							}}
// 							type='button'
// 						/>
// 					</div>
// 				</div>
// 				<div className='description'>
// 					<label htmlFor='description'>Description</label>
// 					<textarea
// 						rows='6'
// 						cols='100'
// 						type='text'
// 						id='description'
// 						placeholder='Enter description'
// 						minLength='2'
// 					></textarea>
// 				</div>

// 				<div className='author'>
// 					<div className='col1'>
// 						<h2>Add author</h2>
// 						<label htmlFor='author_name'>Author name</label>
// 						<div>
// 							<input
// 								type='text'
// 								id='author_name'
// 								placeholder='Enter author name...'
// 							/>
// 						</div>

// 						<Button
// 							className='btn'
// 							type='button'
// 							buttonText='Create author'
// 							trigger={() => {
// 								createAuthor(document.getElementById('author_name').value);
// 							}}
// 						/>
// 						<h2>Duration</h2>
// 						<Input
// 							htmlFor='duration'
// 							labelText='Duration'
// 							type='text'
// 							id='duration'
// 							name='duration'
// 							placeholderText='Enter duration in minutes...'
// 							size='50'
// 							value={duration}
// 							onChange={(e) => setDuration(e.target.value)}
// 						/>

// 						<p>
// 							Duration:
// 							<b>{`${twoDigits(Math.floor(duration / 60))}:${twoDigits(
// 								duration % 60
// 							)}`}</b>
// 							hours
// 						</p>
// 					</div>

// 					<div>
// 						<label>
// 							<h3>Authors</h3>
// 						</label>
// 						<div>{renderAuthorList}</div>
// 						<label>
// 							<h3>Course Authors</h3>
// 						</label>
// 						<div>{renderCourseAuthors}</div>
// 						{/* <input type='submit' value='Submit' /> */}
// 						{/* <button type='button' onClick={testing}>
// 						test
// 					</button> */}
// 					</div>
// 				</div>
// 			</form>
// 		</div>
// 	);
// } else {
// 	if (course.authors !== undefined) {
// 		mockedAuthorsList.forEach((obj) => {
// 			if (course.authors.includes(obj.id)) {
// 				arr.push(`${obj.name}. `);
// 			}
// 		});
// 	}

// 	return (
// 		<div className='createcourse'>
// 			<form>
// 				<div className='title'>
// 					<div className='input'>
// 						<label htmlFor='title'>Title</label>
// 						<input
// 							type='text'
// 							id='title'
// 							placeholder='Enter title...'
// 							size='50'
// 							defaultValue={course.title}
// 						/>
// 					</div>
// 					<div className='btn'>
// 						<Button
// 							buttonText='Update course'
// 							trigger={() => {
// 								updateCourse();
// 								//props.trigger();
// 							}}
// 							type='button'
// 						/>
// 					</div>
// 				</div>
// 				<div className='description'>
// 					<label htmlFor='description'>Description</label>
// 					<textarea
// 						rows='6'
// 						cols='100'
// 						type='text'
// 						id='description'
// 						placeholder='Enter description'
// 						minLength='2'
// 						defaultValue={course.description}
// 					></textarea>
// 				</div>

// 				<div className='author'>
// 					<div className='col1'>
// 						<h2>Add author</h2>
// 						<label htmlFor='author_name'>Author name</label>
// 						<div>
// 							<input
// 								type='text'
// 								id='author_name'
// 								placeholder='Enter author name...'
// 							/>
// 						</div>

// 						<Button
// 							className='btn'
// 							type='button'
// 							buttonText='Create author'
// 							trigger={() => {
// 								createAuthor(document.getElementById('author_name').value);
// 							}}
// 						/>
// 						<h2>Duration</h2>
// 						<Input
// 							htmlFor='duration'
// 							labelText='Duration'
// 							type='number'
// 							id='duration'
// 							name='duration'
// 							placeholderText='Enter duration in minutes...'
// 							size='50'
// 							value={duration}
// 							onChange={(e) => setDuration(e.target.value)}
// 						/>

// 						<p>
// 							Duration:
// 							<b>{`${twoDigits(Math.floor(duration / 60))}:${twoDigits(
// 								duration % 60
// 							)}`}</b>
// 							hours
// 						</p>
// 					</div>

// 					<div>
// 						<label>
// 							<h3>Authors</h3>
// 						</label>
// 						<div>{renderAuthorList}</div>
// 						<label>
// 							<h3>Course Authors</h3>
// 						</label>
// 						<div>{renderCourseAuthors}</div>
// 					</div>
// 				</div>
// 			</form>
// 		</div>
// 	);
// }
// }

function CourseForm() {
	//just a test component so I can wrap my head around things

	let mockedCoursesList = store.getState().courses;
	let mockedAuthorsList = store.getState().author;

	const { courseId } = useParams();
	let history = useHistory();

	const twoDigits = (num) => {
		return num.toString().padStart(2, '0');
	};
	const names = [];
	const newCourse = mockedCoursesList.find((course) => course.id === courseId);
	console.log(newCourse);

	if (courseId !== undefined) {
		newCourse.authors.map((item) => {
			mockedAuthorsList.map((item2) => {
				if (item === item2.id) {
					names.push({ id: item2.id, name: item2.name });
				}
			});
		});
	}

	const cond = courseId === undefined ? [] : names;

	const [courseAuthors, setCourseAuthors] = useState(cond);
	const [authors, setAuthors] = useState('');

	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState('');
	const [newAuthor, setNewAuthor] = useState('');
	const [authorList, setAuthorList] = useState(mockedAuthorsList);

	useEffect(() => {
		//if statement should go here

		if (courseId === undefined) {
			setCourseAuthors(names);
		}
	}, []);

	if (courseId !== undefined) {
		const obj = {
			title: '',
			description: '',
			duration: '',
			authors: [],
		};

		courseAuthors.forEach((item) => obj.authors.push(item.id));

		let renderAuthorList = authorList.map((author) => {
			return (
				<div className='authorlist' key={author.id}>
					<p>{author.name}</p>
					<Button
						type='button'
						buttonText='Add author'
						trigger={() => {
							setCourseAuthors(Array.from(new Set([...courseAuthors, author])));
						}}
					/>
				</div>
			);
		});
		console.log(courseAuthors);
		let renderCourseAuthors = courseAuthors.map((author) => {
			return (
				<div className='authorlist' key={author.id}>
					<p>{author.name}</p>
					<Button
						type='button'
						buttonText='Remove author'
						trigger={() => {
							setCourseAuthors(
								courseAuthors.filter((id) => id.name !== author.name)
							);
						}}
					/>
				</div>
			);
		});

		const updateCourse = () => {
			obj.title = document.getElementById('title').value;
			obj.description = document.getElementById('description').value;
			obj.duration = Number(document.getElementById('duration').value);
			if (obj.title === '' || obj.description === '' || duration === '') {
				alert('Please fill in all fields');
			} else {
				console.log(obj);
				//console.log('authors', authorList);
				putCourse(courseId, obj);

				updateCoursesAction.payload = obj;
				addAuthorAction.payload = authorList;
				store.dispatch(addCoursesAction);
				store.dispatch(addAuthorAction);
				console.log('store', store.getState());

				mockedCoursesList.push(obj);
				console.log(obj);
				console.log(mockedAuthorsList);
				console.log(mockedCoursesList);
				//history.push(`/courses`);
			}
		};

		return (
			<div className='main'>
				<div className='row1'>
					<Input
						htmlFor='title'
						labelText='Title'
						type='text'
						id='title'
						name='title'
						placeholderText='Enter title...'
						size='50'
						defaultValue={newCourse.title}
						// value={title}
						// onChange={(e) => setTitle(e.target.value)}
					/>
					<Button
						className='btn'
						type='button'
						buttonText='Update course'
						trigger={() => {
							updateCourse();
						}}
					/>
				</div>
				<div className='row2'>
					<label htmlFor='description'>Description</label>
					<textarea
						name='description'
						id='description'
						cols='100'
						rows='10'
						defaultValue={newCourse.description}
						// value={description}
						// onChange={(e) => setDescription(e.target.value)}
					></textarea>
				</div>
				<div className='row3'>
					<div className='col1'>
						<h2>Add authors</h2>
						<Input
							htmlFor='createAuthor'
							labelText='Author name'
							type='text'
							id='createAuthor'
							name='createAuthor'
							placeholderText='Enter author name...'
							size='50'
							value={newAuthor}
							onChange={(e) => setNewAuthor(e.target.value)}
						/>
						<Button
							className='btn'
							type='button'
							buttonText='Create author'
							trigger={() => {
								setAuthorList([
									...authorList,
									{ id: uuidv4(), name: newAuthor },
								]);
								postAuthor({ name: newAuthor });
							}}
						/>
						<Input
							htmlFor='duration'
							labelText='Duration'
							type='number'
							id='duration'
							name='duration'
							placeholderText='Enter duration in minutes...'
							size='50'
							defaultValue={newCourse.duration}
							//value={duration}
							onChange={(e) => setDuration(e.target.value)}
						/>
						<p>
							Duration:
							<b>{`${twoDigits(Math.floor(duration / 60))}:${twoDigits(
								duration % 60
							)}`}</b>
							hours
						</p>
					</div>
					<div className='col2'>
						<h2>Authors</h2>
						<div className='list1'>{renderAuthorList}</div>
						<h2>Course Authors</h2>
						<div>{renderCourseAuthors}</div>
					</div>
				</div>
			</div>
		);
	} else {
		const date = new Date();
		const obj = {
			//id: uuidv4(),
			title: title,
			description: description,
			//creationDate: `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`,
			duration: Number(duration),
			authors: [],
		};
		courseAuthors.forEach((item) => obj.authors.push(item.id));

		let renderAuthorList = authorList.map((author) => {
			return (
				<div className='authorlist' key={author.id}>
					<p>{author.name}</p>
					<Button
						type='button'
						buttonText='Add author'
						trigger={() => {
							setCourseAuthors(Array.from(new Set([...courseAuthors, author])));
						}}
					/>
				</div>
			);
		});

		let renderCourseAuthors = courseAuthors.map((author) => {
			return (
				<div className='authorlist' key={author.id}>
					<p>{author.name}</p>
					<Button
						type='button'
						buttonText='Remove author'
						trigger={() => {
							setCourseAuthors(
								courseAuthors.filter((id) => id.name !== author.name)
							);
						}}
					/>
				</div>
			);
		});

		const createcourse = () => {
			if (obj.title === '' || obj.description === '' || duration === '') {
				alert('Please fill in all fields');
			} else {
				postCourse(obj);

				addCoursesAction.payload = obj;
				addAuthorAction.payload = authorList;
				store.dispatch(addCoursesAction);
				store.dispatch(addAuthorAction);
				console.log('store', store.getState());

				mockedCoursesList.push(obj);
				console.log(obj);
				console.log(mockedAuthorsList);
				console.log(mockedCoursesList);
				//history.push(`/courses`);
			}
		};

		return (
			<div className='main'>
				<div className='row1'>
					<Input
						htmlFor='title'
						labelText='Title'
						type='text'
						id='title'
						name='title'
						placeholderText='Enter title...'
						size='50'
						value={title}
						onChange={(e) => setTitle(e.target.value)}
					/>
					<Button
						className='btn'
						type='button'
						buttonText='Create course'
						trigger={() => {
							createcourse();
						}}
					/>
				</div>
				<div className='row2'>
					<label htmlFor='description'>Description</label>
					<textarea
						name='description'
						id='description'
						cols='100'
						rows='10'
						value={description}
						onChange={(e) => setDescription(e.target.value)}
					></textarea>
				</div>
				<div className='row3'>
					<div className='col1'>
						<h2>Add authors</h2>
						<Input
							htmlFor='createAuthor'
							labelText='Author name'
							type='text'
							id='createAuthor'
							name='createAuthor'
							placeholderText='Enter author name...'
							size='50'
							value={newAuthor}
							onChange={(e) => setNewAuthor(e.target.value)}
						/>
						<Button
							className='btn'
							type='button'
							buttonText='Create author'
							trigger={() => {
								setAuthorList([
									...authorList,
									{ id: uuidv4(), name: newAuthor },
								]);
								postAuthor({ name: newAuthor });
							}}
						/>
						<Input
							htmlFor='duration'
							labelText='Duration'
							type='number'
							id='duration'
							name='duration'
							placeholderText='Enter duration in minutes...'
							size='50'
							value={duration}
							onChange={(e) => setDuration(e.target.value)}
						/>
						<p>
							Duration:
							<b>{`${twoDigits(Math.floor(duration / 60))}:${twoDigits(
								duration % 60
							)}`}</b>
							hours
						</p>
					</div>
					<div className='col2'>
						<h2>Authors</h2>
						<div className='list1'>{renderAuthorList}</div>
						<h2>Course Authors</h2>
						<div>{renderCourseAuthors}</div>
					</div>
				</div>
			</div>
		);
	}
}

export default CourseForm;
