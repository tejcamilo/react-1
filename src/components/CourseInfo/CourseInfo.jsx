import Button from '../../common/Button/Button';
import './courseinfo.css';
import { useParams } from 'react-router-dom';
import { mockedAuthorsList } from '../../constants';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Header from '../Header/Header';
import store from '../../store';

function CourseInfo() {
	let mockedCoursesList = store.getState().courses;
	let mockedAuthorsList = store.getState().author;

	const [course, setCourse] = useState('');
	const { courseId } = useParams();

	useEffect(() => {
		const newCourse = mockedCoursesList.find(
			(course) => course.id === courseId
		);

		setCourse(newCourse);
	}, []);

	//console.log(course.authors);
	let authors = [];
	if (course.authors !== undefined) {
		authors = course.authors.map((author) => author);
	}

	let arr = [];
	mockedAuthorsList.forEach((obj) => {
		if (authors.includes(obj.id)) {
			arr.push(`${obj.name}. `);
		}
	});

	let history = useHistory();
	const handleClick = () => {
		history.push('/courses');
		//window.location.replace('/courses'); //had to use this to redirect sine history.push wont work
	};

	return (
		<div className='main'>
			<Button
				className='btn'
				buttonText='Back to courses'
				trigger={handleClick}
			/>
			<h2>{course.title}</h2>
			<div className='courseInfo'>
				<p>{course.description}</p>
				<div className='col2'>
					<p className='courseId'>
						<b>ID:</b>
						{course.id}
					</p>
					<p>
						<b>Duration:</b>
						{`${Math.floor(course.duration / 60)}:${
							course.duration % 60
						} hours`}
					</p>

					<p>
						<b>Created:</b> {course.creationDate}
					</p>
					<p>
						<b>Authors:</b>
						{arr}
					</p>
				</div>
			</div>
		</div>
	);
}

export default CourseInfo;
