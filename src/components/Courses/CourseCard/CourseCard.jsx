import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import Button from '../../../common/Button/Button';
import { mockedAuthorsList } from '../../../constants';
import './coursecard.css';
import { useHistory } from 'react-router-dom';
import store from '../../../store';
import { useState } from 'react';
import { deleteCoursesAction } from '../../../store/courses/reducer';
import { useSelector } from 'react-redux';
import { deleteCourse } from '../../../store/courses/thunk';

function CourseCard(props) {
	useSelector((state) => state);
	//console.log('coursecard', store.getState());
	const authorsList = store.getState().author;
	const twoDigits = (num) => {
		return num.toString().padStart(2, '0');
	};
	const authors = props.authors.map((author) => author);

	let arr = [];
	authorsList.forEach((obj) => {
		if (authors.includes(obj.id)) {
			arr.push(`${obj.name}. `);
		}
	});

	let history = useHistory();
	const handleClick = () => {
		history.push(`/courses/${props.urlId}`);
	};

	const updateCourse = () => {
		history.push(`/courses/update/${props.urlId}`);
	};

	const hanleDelete = () => {
		const serverResponse = deleteCourse(props.urlId);

		if (serverResponse.successful) {
			deleteCoursesAction.payload = props.urlId;
			console.log(store.getState());
			store.dispatch(deleteCoursesAction);
			console.log(deleteCoursesAction);
			console.log(store.getState().courses);
			props.parentFunction();
		}
	};
	if (store.getState().user.role === 'admin') {
		return (
			<div className='course'>
				<div className='col1'>
					<h2>{props.title}</h2>
					<p>{props.description}</p>
					{/* In mockedCoursesList each course has only authors' ids.
					To define authors' names you should find them in mockedAuthorsList by id. */}
				</div>

				<div className='col2'>
					<p className='authors'>
						<b>Authors:</b> {arr}
					</p>
					<p>
						<b>Duration:</b>
						{`${twoDigits(Math.floor(props.duration / 60))}:${twoDigits(
							props.duration % 60
						)} hours`}
					</p>
					<p>
						<b>Created:</b> {props.created}
					</p>
					<div className='buttons'>
						<Button
							className='btn'
							buttonText='Show course'
							trigger={handleClick}
						/>
						<Button
							className='btn'
							buttonText='Update'
							trigger={updateCourse}
						/>
						<Button
							className='btnDelete'
							buttonText='Delete'
							trigger={hanleDelete}
						/>
					</div>
				</div>
			</div>
		);
	} else {
		return (
			<div className='course'>
				<div className='col1'>
					<h2>{props.title}</h2>
					<p>{props.description}</p>
					{/* In mockedCoursesList each course has only authors' ids.
					To define authors' names you should find them in mockedAuthorsList by id. */}
				</div>

				<div className='col2'>
					<p className='authors'>
						<b>Authors:</b> {arr}
					</p>
					<p>
						<b>Duration:</b>
						{`${twoDigits(Math.floor(props.duration / 60))}:${twoDigits(
							props.duration % 60
						)} hours`}
					</p>
					<p>
						<b>Created:</b> {props.created}
					</p>
					<div className='buttons'>
						<Button
							className='btn'
							buttonText='Show course'
							trigger={handleClick}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default CourseCard;
