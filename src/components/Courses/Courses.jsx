import Button from '../../common/Button/Button';
import CourseCard from '../Courses/CourseCard/CourseCard';
import { mockedCoursesList, mockedAuthorsList } from '../../constants';
import SearchBar from './components/SearchBar.jsx'; //why it only works with the extension?
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import './courses.css';
import store from '../../store';
import { getRole } from '../../store/user/thunk';
import { useDispatch, useSelector } from 'react-redux';

function Courses(props) {
	const dispatch = useDispatch();
	useSelector((state) => state);
	useEffect(() => {
		dispatch(getRole());
	}, [dispatch]);

	console.log('from courses', store.getState());

	let mockedCoursesList = store.getState().courses;
	let mockedAuthorsList = store.getState().author;

	const parentFunction = () => {
		setCourses(map(store.getState().courses));
	};

	const map = (arr) => {
		return arr.map((course) => {
			const { id, title, description, authors, duration, creationDate } =
				course; //destructured course array
			return (
				<CourseCard
					urlId={id}
					key={id}
					title={title}
					description={description}
					authors={authors} //need to use authors id to access mockedAuthorsList
					duration={duration}
					created={creationDate}
					parentFunction={parentFunction}
				/>
			);
		});
	};

	let [courses, setCourses] = useState(map(mockedCoursesList));

	let regex = (word, reg) => {
		return word.toLowerCase().includes(reg.toLowerCase());
	};

	const searchCourses = (id) => {
		const newCourses = mockedCoursesList.filter((course) => {
			return regex(course.title, id) || course.id === id;
		});
		//console.log(newCourses);
		setCourses(map(newCourses));
	};
	let history = useHistory();
	const handleClick = () => {
		history.push('/courses/add');
	};

	if (store.getState().user.role === 'admin') {
		return (
			<div className='body'>
				<div className='search'>
					<SearchBar
						trigger={() => {
							searchCourses(document.getElementById('input').value);
						}}
					/>
					<Button
						className='addcourse'
						buttonText='Add new course'
						trigger={handleClick}
					/>
				</div>
				<div>{courses}</div>
			</div>
		);
	} else {
		return (
			<div className='body'>
				<div className='search'>
					<SearchBar
						trigger={() => {
							searchCourses(document.getElementById('input').value);
						}}
					/>
				</div>
				<div>{courses}</div>
			</div>
		);
	}
}

export default Courses;
