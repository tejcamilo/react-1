import Button from '../../../common/Button/Button';
import Input from '../../../common/Input/Input';
import { clickHandler, findCourse } from '../Courses';
import './searchbar.css';

function SearchBar(props) {
	return (
		<div className='input'>
			<Input
				placeholderText='Enter course name...'
				labelText=''
				htmlFor='input'
				type='text'
				id='input'
				name='input'
				//onChange={(e) => setPassword(e.target.value)}
			/>
			<Button
				className='searchbtn'
				buttonText='Search'
				trigger={props.trigger}
			/>
		</div>
	);
}

export default SearchBar;
