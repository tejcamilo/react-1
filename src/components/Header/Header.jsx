import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { useHistory } from 'react-router-dom';
import './header.css';
import store from '../../store';
import { removeUserAction } from '../../store/user/reducer';
import { logout } from '../../store/user/thunk';

function Header() {
	const name = () => {
		if (localStorage.getItem('userToken') === null) {
			return '';
		} else {
			return (
				<div className='left'>
					<h3>{store.getState().user.name}</h3>
					<Button
						className='btn'
						buttonText='Logout'
						type='button'
						trigger={handleClick}
					/>
				</div>
			);
		}
	};

	let history = useHistory();

	const handleClick = () => {
		logout();
		store.dispatch(removeUserAction);
		localStorage.removeItem('userToken');
		localStorage.removeItem('name');
		history.push('/login');
	};

	return (
		<div className='header'>
			<Logo />
			<>{name()}</>
		</div>
	);
}

export default Header;
