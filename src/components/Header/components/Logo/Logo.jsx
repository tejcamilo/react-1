function Logo() {
	return (
		<img
			src='https://www.freeiconspng.com/thumbs/courses-icon/courses-icon-28.png'
			alt='logo'
			width='65px'
		></img>
	);
}
export default Logo;
