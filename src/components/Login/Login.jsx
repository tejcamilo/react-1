import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { useState } from 'react';
import './login.css';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import { getCourses, serverCourses } from '../../services';
import store from '../../store';
import { addUserAction, removeUserAction } from '../../store/user/reducer';
import { Redirect } from 'react-router-dom/cjs/react-router-dom';
import { postLogin } from '../../store/user/thunk';

function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const userLogin = {
		name: '',
		password: password,
		email: email,
		token: '',
	};
	let history = useHistory();
	const redirect = () => {
		if (localStorage.getItem('userToken') !== null) {
			console.log('works');
			//<Redirect to='/courses' />; use this perhaps?
			history.push('/courses');
		}
	};

	redirect();
	const submitHandler = async (e) => {
		e.preventDefault();
		const login = await postLogin(userLogin);
		if (login.successful) {
			localStorage.setItem('userToken', login.result);
			userLogin.name = login.user.name;
			userLogin.token = login.result;
			addUserAction.payload = userLogin;
			store.dispatch(addUserAction);
			console.log(store.getState());
			history.push('/courses');
		} else {
			alert('Cannot login, check console details');
			console.log(login);
		}
	};

	return (
		<div className='login'>
			<h1>Login</h1>
			<form onSubmit={submitHandler}>
				<Input
					htmlFor='email'
					labelText='Email'
					type='text'
					id='email'
					name='email'
					placeholderText='Enter email'
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<Input
					htmlFor='password'
					labelText='Password'
					type='password'
					id='password'
					name='password'
					placeholderText='Enter password'
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>
				<Button type='submit' buttonText='Login' />
			</form>

			<p>
				If you don't have an account, you can
				<Link to='/registration'>Register</Link>
				{/* <a href='/registration'>Register</a> */}
			</p>
		</div>
	);
}
export default Login;
