import { useHistory } from 'react-router-dom';
import store from '../../store';
import { Route, Redirect } from 'react-router-dom';
import Header from '../Header/Header';

function PrivateRoute({ component: Component, ...rest }) {
	// let history = useHistory();
	// if (store.getState().user.role !== 'admin') {
	// 	history.push('/courses');
	// }
	return (
		<Route
			{...rest}
			render={(props) =>
				store.getState().user.role === 'admin' ? (
					<>
						<Header />
						<Component {...props} />
					</>
				) : (
					<Redirect to='/courses' />
				)
			}
		/>
	);
}

export default PrivateRoute;
