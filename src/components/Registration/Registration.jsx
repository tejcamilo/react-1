import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import { Link } from 'react-router-dom';
import './registration.css';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { postUser } from '../../store/user/thunk';

function Registration() {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const newUser = {
		name: name,
		password: password,
		email: email,
	};

	const submitHandler = (e) => {
		e.preventDefault();
		postUser(newUser);
	};

	let history = useHistory();

	const handleClick = () => {
		history.push('/login');
	};

	return (
		<div className='registration'>
			<h1>Registration</h1>
			<form onSubmit={submitHandler}>
				<div>
					<Input
						htmlFor='name'
						labelText='Name'
						type='text'
						id='name'
						name='name'
						placeholderText='Enter name'
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
				</div>
				<div>
					<Input
						htmlFor='email'
						labelText='Email'
						type='email'
						id='email'
						name='email'
						placeholderText='Enter email'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
				</div>
				<div>
					<Input
						htmlFor='password'
						labelText='Password'
						type='password'
						id='password'
						name='password'
						placeholderText='Enter password'
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
				</div>
				<br />
				<Button type='submit' buttonText='Registration' trigger={handleClick} />
			</form>

			<p>
				If you have an account, you can<Link to='/login'>Login</Link>
			</p>
		</div>
	);
}

export default Registration;
