import store from './store';
import { getCoursesAction } from './store/courses/reducer';
import { getAuthorAction } from './store/author/reducer';

try {
	let course;
	let request = new XMLHttpRequest();
	request.open('GET', 'http://localhost:4000/courses/all', false);
	request.send(null);

	if (request.status === 200) {
		course = JSON.parse(request.responseText).result;
	}
	// getCoursesAction.payload = course;
	// store.dispatch(getCoursesAction);

	course.map((item) => {
		getCoursesAction.payload = item;
		store.dispatch(getCoursesAction);
	});
	request = new XMLHttpRequest();
	request.open('GET', 'http://localhost:4000/authors/all', false);
	request.send(null);

	const authors = JSON.parse(request.responseText).result;

	authors.map((item) => {
		getAuthorAction.payload = item;
		store.dispatch(getAuthorAction);
	});
} catch (error) {
	console.error(error);
	// Expected output: ReferenceError: nonExistentFunction is not defined
	// (Note: the exact output may be browser-dependent)
}
