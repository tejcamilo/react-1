const authorsInitialState = [];
export const addAuthorAction = {
	type: 'author/addAuthor',
	payload: '',
};

export const removeAuthorAction = {
	type: 'author/removeAuthor',
	payload: {
		title: 'server response goes here',
	},
};
export const getAuthorAction = {
	type: 'author/getAuthor',
	payload: {
		title: '',
	},
};

function authorReducer(state = authorsInitialState, action) {
	switch (action.type) {
		case 'author/getAuthor': {
			return [...state, action.payload];
		}
		case 'author/addAuthor': {
			return action.payload;
		}
		case 'author/removeAuthor': {
			return state.filter((author) => author.id !== action.payload.id);
		}
		default:
			return state;
	}
}

export default authorReducer;
