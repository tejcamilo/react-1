import store from '../index';

export const postAuthor = async (object) => {
	const addAuthor = fetch('http://localhost:4000/authors/add', {
		method: 'POST',
		body: JSON.stringify(object),
		headers: {
			'Content-Type': 'application/json',
			Authorization: store.getState().user.token,
		},
	})
		.then((response) => response.json())
		.then((result) => result);

	const a = await addAuthor;
	console.log(a);
};
