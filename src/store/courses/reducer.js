const coursesInitialState = [];

export const addCoursesAction = {
	type: 'courses/addCourses',
	payload: '',
};

export const deleteCoursesAction = {
	type: 'courses/deleteCourses',
	payload: '',
};

export const updateCoursesAction = {
	type: 'courses/updateCourses',
	payload: '',
};

export const getCoursesAction = {
	type: 'courses/getCourses',
	payload: '',
};

function coursesReducer(state = coursesInitialState, action) {
	switch (action.type) {
		case 'courses/getCourses': {
			return [...state, action.payload];
		}
		case 'courses/addCourses': {
			return [...state, action.payload];
		}
		case 'courses/deleteCourses': {
			return state.filter((course) => course.id !== action.payload);
		}
		case 'courses/updateCourses': {
			return [
				state.filter((course) => course.id !== action.payload.id),
				action.payload,
			];
		}
		default:
			return state;
	}
}

export default coursesReducer;
