import store from '../index';

export const postCourse = async (object) => {
	const addCourse = fetch('http://localhost:4000/courses/add', {
		method: 'POST',
		body: JSON.stringify(object),
		headers: {
			'Content-Type': 'application/json',
			Authorization: store.getState().user.token,
		},
	})
		.then((response) => response.json())
		.then((result) => result);

	const a = await addCourse;
	console.log(a);
};

export const deleteCourse = async (id) => {
	const deleteCourse = fetch(`http://localhost:4000/courses/${id}`, {
		method: 'DELETE',
		headers: {
			Authorization: store.getState().user.token,
		},
	})
		.then((response) => response.json())
		.then((result) => result);

	const a = await deleteCourse;

	if (a.successful) {
		console.log(a);
		return a;
	} else {
		alert('Error deleting course, check logs');
		console.log(a);
	}
};

export const putCourse = async (id, object) => {
	const updateCourse = fetch(`http://localhost:4000/courses/${id}`, {
		method: 'PUT',
		body: JSON.stringify(object),
		headers: {
			'Content-Type': 'application/json',
			Authorization: store.getState().user.token,
		},
	})
		.then((response) => response.json())
		.then((result) => result);

	const a = await updateCourse;
	console.log(a);
};
