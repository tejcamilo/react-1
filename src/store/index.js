import { createStore, combineReducers, applyMiddleware } from 'redux';
import authorReducer from './author/reducer';
import coursesReducer from './courses/reducer';
import userReducer from './user/reducer';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
	author: authorReducer,
	courses: coursesReducer,
	user: userReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
