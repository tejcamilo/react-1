const initialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export const addUserAction = {
	type: 'user/addUser',
	payload: '',
};

export const removeUserAction = {
	type: 'user/removeUser',
	payload: '',
};

export const getUserRole = {
	type: 'user/getUserRole',
	payload: '',
};

function userReducer(state = initialState, action) {
	switch (action.type) {
		case 'user/addUser': {
			return {
				...state,

				isAuth: true,
				name: action.payload.name,
				email: action.payload.email,
				token: action.payload.token,
			};
		}
		case 'user/removeUser': {
			return {
				...state,

				isAuth: false,
				name: '',
				email: '',
				token: '',
				role: '',
			};
		}
		case 'user/getUserRole': {
			return {
				...state,
				role: action.payload,
			};
		}
		default:
			return state;
	}
}

export default userReducer;
