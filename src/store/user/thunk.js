import store from '../index';
import { getUserRole } from '../user/reducer';

export const postLogin = async (obj) => {
	const addCourse = fetch('http://localhost:4000/login', {
		method: 'POST',
		body: JSON.stringify(obj),
		headers: {
			'Content-Type': 'application/json',
		},
	})
		.then((response) => response.json())
		.then((result) => result);

	const a = await addCourse;
	console.log(a);
	if (a.successful) {
		return a;
	} else {
		alert('Somehting went wrong, user not registered!');
	}
};

export const postUser = async (obj) => {
	const addCourse = fetch('http://localhost:4000/register', {
		method: 'POST',
		body: JSON.stringify(obj),
		headers: {
			'Content-Type': 'application/json',
		},
	})
		.then((response) => response.json())
		.then((result) => result);

	const a = await addCourse;
	console.log(a);
	if (a.successful) {
		console.log('User registered succesfully');
	} else {
		alert('Somehting went wrong, user not registered!');
	}
};

export const getRole = () => {
	return async () => {
		const response = await fetch('http://localhost:4000/users/me', {
			method: 'GET',
			headers: {
				Authorization: store.getState().user.token,
			},
		});
		const a = await response.json();
		if (a.successful) {
			getUserRole.payload = a.result.role;
			store.dispatch(getUserRole);
			console.log('saved role to store');
		}
	};
};

export const logout = async () => {
	const response = await fetch('http://localhost:4000/logout', {
		method: 'DELETE',
		headers: {
			Authorization: store.getState().user.token,
		},
	});
	const result = await response;
	console.log(result);
};
